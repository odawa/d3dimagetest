﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;

namespace D3DImageTest
{
    public class RendererVM : BindableBase
    {
        private MultipleRendering.RendererBase Renderer { get; set; }
        public RendererVM(MultipleRendering.RendererBase renderer)
        {
            Renderer = renderer;
            Renderer.NewFrame += OnNewFrame;
        }

        private void OnNewFrame()
        {
            Application app = Application.Current;
            if (app == null) return;
            Dispatcher d = app.Dispatcher;
            if (d != null)
            {
                d.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    Source = Renderer.GetImageSource();
                }));
            }
        }

        private ImageSource _source;
        public ImageSource Source
        {
            get { return _source; }
            private set
            {
                SetProperty<ImageSource>(ref _source, value);
            }
        }

        public void Stop()
        {
            Renderer.NewFrame -= OnNewFrame;
        }
    }
}
