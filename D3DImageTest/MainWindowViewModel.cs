﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace D3DImageTest
{
    public class MainWindowViewModel : BindableBase
    {
        private MultipleRendering.RendererManager manager;

        private RendererVM[] _vms = new RendererVM[8];
        public RendererVM[] VMS
        {
            get { return _vms; }
        }

        private Methods _method;
        public Methods Method
        {
            get { return _method; }
            set { SetProperty<Methods>(ref _method, value); }
        }

        public MainWindowViewModel(Resolutions resolution, Methods method)
        {
            this.Method = method;
            manager = new MultipleRendering.RendererManager(ToCliResolutions(resolution));
            bool initResult = manager.Init();
            if (!initResult)
            {
                MessageBox.Show(manager.GetLastError());
                Application.Current.Shutdown();
            }
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    if (method == Methods.WB)
                    {
                        _vms[i] = new RendererVM(manager.NewWBRenderer());
                    }
                    else
                    {
                        _vms[i] = new RendererVM(manager.NewD3DRenderer());
                    }
                }
            }
        }

        private MultipleRendering.Resolutions ToCliResolutions(Resolutions resolution)
        {
            MultipleRendering.Resolutions cliResolution = MultipleRendering.Resolutions.Mix;
            switch (resolution)
            {
                case Resolutions.Low:
                    cliResolution = MultipleRendering.Resolutions.Low;
                    break;
                case Resolutions.High:
                    cliResolution = MultipleRendering.Resolutions.High;
                    break;
                case Resolutions.Mix:
                    cliResolution = MultipleRendering.Resolutions.Mix;
                    break;
                default:
                    break;
            }
            return cliResolution;
        }

        public void Stop()
        {
            foreach (var vm in VMS)
            {
                if (vm != null)
                {
                    vm.Stop();
                }
            }
        }
    }
}
