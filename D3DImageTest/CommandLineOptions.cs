﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D3DImageTest
{
    public enum Resolutions
    {
        Low,
        High,
        Mix
    }

    public enum Methods
    {
        WB,
        D3D
    }

    public class CommandLineOptions
    {
        [CommandLine.Option('r', DefaultValue=Resolutions.Mix)]
        public Resolutions Resolution { get; set; }

        [CommandLine.Option('m', DefaultValue=Methods.D3D)]
        public Methods Method { get; set; }
    }
}
