﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace D3DImageTest
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            this.DispatcherUnhandledException += Application_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            Methods method = Methods.D3D;
            Resolutions resolution = Resolutions.Mix;

            CommandLineOptions clo = new CommandLineOptions();
            if (CommandLine.Parser.Default.ParseArguments(e.Args, clo))
            {
                method = clo.Method;
                resolution = clo.Resolution;
            }

            MainWindow mainWindow = new MainWindow(new MainWindowViewModel(resolution, method));
            mainWindow.Show();
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(BuildExceptionMessage(e.ExceptionObject), "CurrentDomain_UnhandledException");
            Shutdown();
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            MessageBox.Show(BuildExceptionMessage(e.Exception), "Application_DispatcherUnhandledException");
            Shutdown();
        }

        private string BuildExceptionMessage(object exceptionObj)
        {
            string exType = "unknown type";
            string exMessage = "????";
            Exception exception = exceptionObj as Exception;
            if (exception != null)
            {
                exType = exception.GetType().ToString();
                exMessage = exception.Message;
            }

            string msg = string.Format("Type : [{0}] Message : [{1}]", exType, exMessage);
            return msg;
        }
    }
}
