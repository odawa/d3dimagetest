#pragma once

#include <vector>
#include <d3d9.h>
#include "ref_shared_ptr.h"

using namespace System;
using namespace System::Windows::Interop;
using namespace System::Windows::Media;
using namespace System::Windows::Media::Imaging;

namespace MultipleRendering
{
	public ref class FrameInfo
	{
	private:
		int m_width;
		int m_height;
		vrms5tools::ref_shared_ptr<std::vector<byte>> pixels;
	public:
		FrameInfo();
		void Update(int width, int height, vrms5tools::ref_shared_ptr<std::vector<byte>>% srcPixels);
		~FrameInfo();
		!FrameInfo();
		int GetWidth() { return m_width; }
		int GetHeight() { return m_height; }
		vrms5tools::ref_shared_ptr<std::vector<byte>>% GetPixels() { return pixels; }
	};

	public ref class RendererBase abstract
	{
	internal:
		virtual void Render(FrameInfo^ frameInfo) = 0;
		bool isDirty = false;
	protected:
		FrameInfo^ currentFrame;
	public:
		Action^ NewFrame;
		virtual ImageSource^ GetImageSource() = 0;
	};

	public ref class D3DRenderer : public RendererBase
	{
	private:
		HWND m_hDeviceWindow;
		unsigned int m_width;
		unsigned int m_height;

		IDirect3DDevice9Ex* m_d3dDevice;
		IDirect3DSwapChain9* m_d3dSwapChain = NULL;
		D3DImage^ m_d3dImage;

		// pixelsへのアクセスを排他制御するためのロックオブジェクト
		Object^ frameLock = gcnew Object();

		HRESULT CreateSwapChain(int width, int height);
	internal:
		virtual void Render(FrameInfo^ frameInfo) override;

	public:
		D3DRenderer(IDirect3DDevice9Ex* device, HWND hDeviceWindow);
		~D3DRenderer();
		!D3DRenderer();
		virtual ImageSource^ GetImageSource() override;
	};

	public ref class WBRenderer : public RendererBase
	{
	private:
		unsigned int m_width;
		unsigned int m_height;
		// WriteableBitmapへの読み書きを排他制御するためのロックオブジェクト
		Object^ lockObj = gcnew Object();
		WriteableBitmap^ wBmp;
	internal:
		virtual void Render(FrameInfo^ frameInfo) override;

	public:
		WBRenderer();
		~WBRenderer();
		!WBRenderer();
		virtual ImageSource^ GetImageSource() override;
	};
}
