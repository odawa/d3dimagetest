#include "stdafx.h"
#include "Renderer.h"
#include <msclr/lock.h>


MultipleRendering::FrameInfo::FrameInfo()
	: pixels(new std::vector<byte>())
{
}

void MultipleRendering::FrameInfo::Update(int width, int height, vrms5tools::ref_shared_ptr<std::vector<byte>>% srcPixels)
{
	m_width = width;
	m_height = height;
	auto newSize = srcPixels->size();
	if (pixels->size() != newSize)
	{
		pixels->resize(newSize);
	}

	memcpy_s(pixels->data(), newSize, srcPixels->data(), newSize);
}


MultipleRendering::FrameInfo::~FrameInfo()
{
	this->!FrameInfo();
}

MultipleRendering::FrameInfo::!FrameInfo()
{

}

MultipleRendering::D3DRenderer::D3DRenderer(IDirect3DDevice9Ex* device, HWND hDeviceWindow)
	: m_hDeviceWindow(hDeviceWindow)
{
	if (device == NULL)
	{
		throw std::invalid_argument("device is null.");
	}
	currentFrame = gcnew FrameInfo();
	
	m_d3dDevice = device;
	m_d3dDevice->AddRef();

	m_d3dImage = gcnew D3DImage();
}

void MultipleRendering::D3DRenderer::Render(FrameInfo^ newFrameInfo)
{
	msclr::lock fl(frameLock);
	currentFrame->Update(newFrameInfo->GetWidth(), newFrameInfo->GetHeight(), newFrameInfo->GetPixels());

	if (!isDirty)
	{
		isDirty = true;
		if (NewFrame != nullptr)
		{
			NewFrame();
		}
	}
}

HRESULT MultipleRendering::D3DRenderer::CreateSwapChain(int width, int height)
{
	m_width = width;
	m_height = height;

	// 既にSwapChainを使っていたら、その参照カウントを減らす
	if (m_d3dSwapChain != NULL)
	{
		m_d3dSwapChain->Release();
	}

	D3DPRESENT_PARAMETERS params;
	ZeroMemory(&params, sizeof(D3DPRESENT_PARAMETERS));
	params.BackBufferWidth = m_width;
	params.BackBufferHeight = m_height;
	params.BackBufferFormat = D3DFORMAT::D3DFMT_X8R8G8B8;
	params.BackBufferCount = 1;
	params.MultiSampleType = D3DMULTISAMPLE_NONE;
	params.SwapEffect = D3DSWAPEFFECT::D3DSWAPEFFECT_DISCARD;
	params.hDeviceWindow = m_hDeviceWindow;
	params.Windowed = TRUE;
	params.EnableAutoDepthStencil = false;
	params.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;

	pin_ptr<IDirect3DSwapChain9*> pin_d3dSwapChain = &m_d3dSwapChain;
	IDirect3DSwapChain9** pp_d3dSwapChain = pin_d3dSwapChain;
	HRESULT hr = m_d3dDevice->CreateAdditionalSwapChain(&params, pp_d3dSwapChain);
	return hr;
}

ImageSource^ MultipleRendering::D3DRenderer::GetImageSource()
{
	msclr::lock fl(frameLock);
	if (isDirty)
	{
		isDirty = false;

		// SwapChainがまだない or 映像フレームのサイズとSwapChainのサイズが異なる場合は、新しくSwapChainを作る
		int frameWidth = currentFrame->GetWidth();
		int frameHeight = currentFrame->GetHeight();
		HRESULT hr = D3D_OK;
		if (m_d3dSwapChain == NULL || m_width != frameWidth || m_height != frameHeight)
		{
			hr = CreateSwapChain(frameWidth, frameHeight);
		}

		if (hr == D3D_OK)
		{
			// 現在のRenderTarget（書き込み処理の対象）を退避
			IDirect3DSurface9* p_oldSurf = NULL;
			if (D3D_OK != m_d3dDevice->GetRenderTarget(0, &p_oldSurf))
			{
				p_oldSurf = NULL;
			}

			IDirect3DSurface9* p_surf;
			m_d3dSwapChain->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &p_surf);

			// Deviceに対して、SwapChainのBackBufferを書き込み先として設定する
			m_d3dDevice->SetRenderTarget(0, p_surf);

			D3DLOCKED_RECT lockedRect;
			HRESULT lockResult = p_surf->LockRect(&lockedRect, NULL, D3DLOCK_DISCARD);
			if (lockResult == D3D_OK)
			{
				FillMemory(lockedRect.pBits, lockedRect.Pitch * m_height, 0xFF);
				byte* p_buff = (byte*)lockedRect.pBits;
				byte* p_pixels = currentFrame->GetPixels()->data();
				const int bytesPerPixel = 4;

				// LockRectで取得される領域は、指定されたサイズより大きくなるため、指定されたサイズからはみ出るバイト数を計算
				// 一行分書き込んだら、このバイト数分、書き込み先を飛ばす
				int padding = lockedRect.Pitch - m_width * bytesPerPixel;

				m_d3dDevice->BeginScene();

				for (unsigned int y = 0; y < m_height; y++)
				{
					// 一行分の書き込み
					for (unsigned int x = 0; x < m_width; x++)
					{
						(*p_buff++) = p_pixels[0];
						(*p_buff++) = p_pixels[1];
						(*p_buff++) = p_pixels[2];
						(*p_buff++) = p_pixels[3];
						p_pixels += bytesPerPixel;
					}

					p_buff += padding;	// はみ出た部分に書き込まないように、書き込み先のアドレスを先に飛ばす（次の行の先頭を指すようにする）
				}
				m_d3dDevice->EndScene();

				p_surf->UnlockRect();
			}

			// RenderTargetを元に戻す
			if (p_oldSurf != NULL)
			{
				m_d3dDevice->SetRenderTarget(0, p_oldSurf);
				p_oldSurf->Release();
			}

			m_d3dImage->Lock();
			m_d3dImage->SetBackBuffer(D3DResourceType::IDirect3DSurface9, (System::IntPtr)p_surf, true);
			m_d3dImage->AddDirtyRect(System::Windows::Int32Rect(0, 0, m_width, m_height));
			m_d3dImage->Unlock();

			p_surf->Release();
			m_d3dSwapChain->Present(NULL, NULL, NULL, NULL, 0);
		}
	}
	return m_d3dImage;
}


MultipleRendering::D3DRenderer::~D3DRenderer()
{
	this->!D3DRenderer();
}

MultipleRendering::D3DRenderer::!D3DRenderer()
{
	ULONG devCount, scCount;
	if(m_d3dSwapChain != NULL)
	{
		scCount = m_d3dSwapChain->Release();
	}

	if (m_d3dDevice != NULL)
	{
		devCount = m_d3dDevice->Release();
	}
}

MultipleRendering::WBRenderer::WBRenderer()
{
	currentFrame = gcnew FrameInfo();
}

void MultipleRendering::WBRenderer::Render(FrameInfo^ frameInfo)
{
	msclr::lock l(lockObj);
	currentFrame->Update(frameInfo->GetWidth(), frameInfo->GetHeight(), frameInfo->GetPixels());

	if (!isDirty)
	{
		isDirty = true;
		if (NewFrame != nullptr)
		{
			NewFrame();
		}
	}
}

ImageSource^ MultipleRendering::WBRenderer::GetImageSource()
{
	msclr::lock l(lockObj);

	auto timeout = System::Windows::Duration(System::TimeSpan::FromMilliseconds(10));
	if (isDirty)
	{
		int frameWidth = currentFrame->GetWidth();
		int frameHeight = currentFrame->GetHeight();
		if (wBmp == nullptr || wBmp->PixelWidth != frameWidth || wBmp->PixelHeight != frameHeight)
		{
			if (wBmp != nullptr)
			{
				wBmp->Freeze();
				wBmp = nullptr;
			}
			m_width = frameWidth;
			m_height = frameHeight;
			wBmp = gcnew WriteableBitmap(m_width, m_height, 1.0, 1.0, PixelFormats::Pbgra32, nullptr);
		}

		isDirty = false;
		if (wBmp->TryLock(timeout))  // 実際にロックが取れないことがあるらしい
		{
			byte* p_wBmpBuff = (byte*)wBmp->BackBuffer.ToPointer();
			byte* p_pixels = currentFrame->GetPixels()->data();
			const int bytesPerPixel = 4;

			for (int x = 0; x < m_width; x++)
			{
				for (int y = 0; y < m_height; y++)
				{
					(*p_wBmpBuff++) = p_pixels[0];
					(*p_wBmpBuff++) = p_pixels[1];
					(*p_wBmpBuff++) = p_pixels[2];
					(*p_wBmpBuff++) = p_pixels[3];
					p_pixels += bytesPerPixel;
				}
			}

			wBmp->AddDirtyRect(System::Windows::Int32Rect(0, 0, m_width, m_height));
			wBmp->Unlock();
		}
	}
	return wBmp;
}

MultipleRendering::WBRenderer::~WBRenderer()
{
	this->!WBRenderer();
}

MultipleRendering::WBRenderer::!WBRenderer()
{

}
