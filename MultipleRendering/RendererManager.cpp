#include "stdafx.h"
#include "Windows.h"

#include "RendererManager.h"

using namespace System::IO;

MultipleRendering::RendererSlot::RendererSlot(RendererBase^ renderer, double interval, List<MultipleRendering::FrameInfo^>^ frameInfoList)
	: m_renderer(renderer)
	, m_frameInfoList(frameInfoList)
{
	m_frameIndex = m_random->Next(m_frameInfoList->Count);
	m_timer = gcnew Timer(interval);
	m_timer->Elapsed += gcnew ElapsedEventHandler(this, &RendererSlot::OnElapsed);
	m_timer->Start();
}

void MultipleRendering::RendererSlot::OnElapsed(Object^ sender, ElapsedEventArgs^ e)
{
	RenderNext();
}

MultipleRendering::RendererBase^ MultipleRendering::RendererSlot::GetRenderer()
{
	return m_renderer;
}

void MultipleRendering::RendererSlot::RenderNext()
{
	this->GetRenderer()->Render(m_frameInfoList[m_frameIndex]);
	m_frameIndex = (m_frameIndex + 1) % m_frameInfoList->Count;
}

MultipleRendering::RendererManager::RendererManager(MultipleRendering::Resolutions resolution)
	: m_resolution(resolution)
{
	m_renderers = gcnew Dictionary<RendererBase^, RendererSlot^>();
}

bool MultipleRendering::RendererManager::Init()
{
	HRESULT ensureD3DResult = EnsureD3D();
	if (ensureD3DResult != S_OK)
	{
		m_lastError = "EnsureD3D Failed.";
		return false;
	}

	HRESULT ensureDeviceResult = EnsureDevice();
	if (ensureDeviceResult != S_OK)
	{
		m_lastError = "EnsureDevice Failed.";
		return false;
	}

	EnsureDummyFrames();
	return true;
}

void MultipleRendering::RendererManager::EnsureDummyFrames()
{
	dummyFrameInfoList = gcnew List<MultipleRendering::FrameInfo^>();
	String^ dummyFramesDirPath = String::Concat(System::Windows::Forms::Application::StartupPath, "\\dummyFrames");

	if (m_resolution == MultipleRendering::Resolutions::Mix || m_resolution == MultipleRendering::Resolutions::Low)
	{
		LoadFrames(String::Format("{0}\\640x480", dummyFramesDirPath), 640, 480);
	}
	
	if (m_resolution == MultipleRendering::Resolutions::Mix || m_resolution == MultipleRendering::Resolutions::High)
	{
		LoadFrames(String::Format("{0}\\1280x960", dummyFramesDirPath), 1280, 960);
	}
}

void MultipleRendering::RendererManager::LoadFrames(String^ dirPath, int width, int height)
{
	auto datFiles = Directory::EnumerateFiles(dirPath, "*.dat");

	for each (String^ datFile in datFiles)
	{
		vrms5tools::ref_shared_ptr<std::vector<byte>>% df = vrms5tools::ref_shared_ptr<std::vector<byte>>(new std::vector<byte>());
		LoadFrame(datFile, width, height, df);
		FrameInfo^ info = gcnew FrameInfo();
		info->Update(width, height, df);
		dummyFrameInfoList->Add(info);
	}
}

void MultipleRendering::RendererManager::LoadFrame(String^ path, int width, int height, vrms5tools::ref_shared_ptr<std::vector<byte>>% buff)
{
	int buffSize = width * height * 4;
	buff->resize(buffSize);
	FileStream^ fs = nullptr;
	try
	{
		fs = gcnew FileStream(path, FileMode::Open);
		BinaryReader^ br = gcnew BinaryReader(gcnew BufferedStream(fs));

		array<byte>^ readBuff = gcnew array<byte>(buffSize);
		byte* p_dummyFrameData = buff->data();

		int readSize = br->Read(readBuff, 0, buffSize);

		for (long long i = 0; i < Math::Min(buffSize, readSize); i++)
		{
			p_dummyFrameData[i] = readBuff[i];
		}
	}
	finally
	{
		if (fs != nullptr)
		{
			fs->Close();
		}
	}
}

MultipleRendering::RendererManager::~RendererManager()
{
	this->!RendererManager();
}

MultipleRendering::RendererManager::!RendererManager()
{
	ULONG devCount, d3dCount;
	if (m_d3dDeviceEx != NULL)
	{
		devCount = m_d3dDeviceEx->Release();
	}

	if (m_d3dEx != NULL)
	{
		d3dCount = m_d3dEx->Release();
	}

	if (hDeviceWindow != NULL)
	{
		DestroyWindow(hDeviceWindow);
	}
}

MultipleRendering::D3DRenderer^ MultipleRendering::RendererManager::NewD3DRenderer()
{
	D3DRenderer^ r = gcnew D3DRenderer(m_d3dDeviceEx, hDeviceWindow);
	RendererSlot^ slot = gcnew RendererSlot(r, 60, dummyFrameInfoList);

	m_renderers->Add(r, slot);

	return r;
}

MultipleRendering::WBRenderer^ MultipleRendering::RendererManager::NewWBRenderer()
{
	WBRenderer^ r = gcnew WBRenderer();
	RendererSlot^ slot = gcnew RendererSlot(r, 60, dummyFrameInfoList);

	m_renderers->Add(r, slot);

	return r;
}

HRESULT MultipleRendering::RendererManager::EnsureD3D()
{
	pin_ptr<IDirect3D9Ex*> pin_d3dEx = &m_d3dEx;
	IDirect3D9Ex** pp_d3dE = pin_d3dEx;
	HRESULT hr = Direct3DCreate9Ex(D3D_SDK_VERSION, pp_d3dE);
	return hr;
}

HRESULT MultipleRendering::RendererManager::EnsureDevice()
{
	HWND hDeviceWindow = CreateHWND();
	if (hDeviceWindow == NULL)
	{
		throw gcnew System::Exception("CreateHWND failed.");
	}

	D3DPRESENT_PARAMETERS d3dPresentParams;
	ZeroMemory(&d3dPresentParams, sizeof(D3DPRESENT_PARAMETERS));
	d3dPresentParams.Windowed = true;
	d3dPresentParams.BackBufferWidth = 1;
	d3dPresentParams.BackBufferHeight = 1;
	d3dPresentParams.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dPresentParams.SwapEffect = D3DSWAPEFFECT::D3DSWAPEFFECT_DISCARD;
	d3dPresentParams.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
	d3dPresentParams.EnableAutoDepthStencil = false;

	pin_ptr<IDirect3DDevice9Ex*> pin_d3dDeviceEx = &m_d3dDeviceEx;
	IDirect3DDevice9Ex** pp_d3dDeviceEx = pin_d3dDeviceEx;

	HRESULT hr = m_d3dEx->CreateDeviceEx(
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hDeviceWindow,
		EnsureBehaviorFlags(),
		&d3dPresentParams,
		NULL,
		pp_d3dDeviceEx
		);

	return hr;
}

HWND MultipleRendering::RendererManager::CreateHWND()
{
	WNDCLASSEX wndclassEx;

	// Windowsクラス名がかぶるとRegisterClassExで失敗するので、GUIDを付加して重複しないように配慮
	const wchar_t CLASS_NAME[] = L"WindowForD3DDevice_3784ED33-5902-4882-98C9-81F7532430CA";

	wndclassEx.cbSize = sizeof(WNDCLASSEX);
	wndclassEx.style = CS_HREDRAW | CS_VREDRAW;
	wndclassEx.lpfnWndProc = DefWindowProc;
	wndclassEx.cbClsExtra = 0;
	wndclassEx.cbWndExtra = 0;
	wndclassEx.hInstance = NULL;
	wndclassEx.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclassEx.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclassEx.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclassEx.lpszMenuName = NULL;
	wndclassEx.lpszClassName = CLASS_NAME;
	wndclassEx.hIconSm = NULL;

	if (!RegisterClassEx(&wndclassEx))
	{
		return NULL;
	}

	this->hDeviceWindow = CreateWindowEx(
		WS_EX_LEFT,
		CLASS_NAME,
		TEXT("D3DImageSample"),
		WS_OVERLAPPEDWINDOW,
		0,                   // Initial X
		0,                   // Initial Y
		0,                   // Width
		0,                   // Height
		NULL,
		NULL,
		NULL,
		NULL);

	return this->hDeviceWindow;
}

DWORD MultipleRendering::RendererManager::EnsureBehaviorFlags()
{
	D3DCAPS9 caps;
	HRESULT hr = m_d3dEx->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);

	if (hr != D3D_OK)
	{
		throw gcnew System::Exception("EnsureBehaviorFlags failed.");
	}

	DWORD flags = 0;

	if ((caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT) == D3DDEVCAPS_HWTRANSFORMANDLIGHT)
	{
		flags = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	}
	else
	{
		flags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	return (flags | D3DCREATE_MULTITHREADED | D3DCREATE_FPU_PRESERVE | D3DCREATE_DISABLE_DRIVER_MANAGEMENT);
}
