#pragma once

#include <memory>

namespace vrms5tools
{

	template <typename T>
	public ref class ref_shared_ptr
	{
	public:
		typedef T element_type;

		ref_shared_ptr()
			: ref_shared_ptr(nullptr)
		{

		}

		ref_shared_ptr(nullptr_t)
			: sptr(new std::shared_ptr<T>())
		{

		}

		template <typename D>
		ref_shared_ptr(nullptr_t, D d)
			: sptr(new std::shared_ptr<D>(nullptr, d))
		{

		}

		template <typename D, typename A>
		ref_shared_ptr(nullptr_t, D d, A alloc)
			: sptr(new std::shared_ptr<D>(nullptr, d, alloc))
		{

		}

		template <typename O>
		explicit ref_shared_ptr(O* p)
			: sptr(new std::shared_ptr<T>(p))
		{

		}

		template <typename O, typename D>
		ref_shared_ptr(O* p, D dtor)
			: sptr(new std::shared_ptr<T>(p, dtor))
		{

		}

		template <typename O, typename D, typename A>
		ref_shared_ptr(O* p, D dtor, A accloc)
			: sptr(new std::shared_ptr<T>(p, dtor, alloc))
		{

		}

		template <typename O>
		ref_shared_ptr(ref_shared_ptr<O> rsp)
			: ref_shared_ptr(rsp.toStd())
		{

		}

		template <typename O>
		explicit ref_shared_ptr(std::shared_ptr<O> const& sp)
			: sptr(new sshared_ptr<T>(sp))
		{

		}

		template <typename O>
		explicit ref_shared_ptr(std::shared_ptr<O>&& sp)
			: sptr(new std::shared_ptr<T>(sp))
		{

		}

		// auto_ptr からの変換は用意していない
		// weak_ptr からの変換は用意していない
		// unique_ptr からの変換は用意していない


		~ref_shared_ptr()
		{
			delete sptr;
			sptr = nullptr;
		}

		!ref_shared_ptr()
		{
			delete sptr;
			sptr = nullptr;
		}

		ref_shared_ptr^ const operator=(ref_shared_ptr const rsp)
		{
			*sptr = *rep.sptr;
			return this;
		}

		template <typename O>
		ref_shared_ptr^ const operator=(ref_shared_ptr<O> const rsp)
		{
			*sptr = *rsp.sptr;
			return this;
		}

		ref_shared_ptr^ const operator=(std::shared_ptr<T> const& sp)
		{
			*sptr = sp;
			return this;
		}

		template <typename O>
		ref_shared_ptr^ const operator=(std::shared_ptr<O> const& sp)
		{
			*sptr = sp;
			return this;
		}

		template <typename O>
		ref_shared_ptr^ const operator=(std::shared_ptr<O>&& sp)
		{
			*sptr = sp;
			return this;
		}

		// auto_ptr の代入は用意していない
		// weak_ptr の代入は用意していない
		// unique_ptr の代入は用意していない

		void reset()
		{
			sptr->reset();
		}

		template <typename O>
		void reset(O* p)
		{
			sptr->reset(p);
		}

		template <typename O, typename D>
		void reset(O* p, D dtor)
		{
			sptr->reset(p, dtor);
		}

		template <typename O, typename D, typename A>
		void reset(O* p, D dtor, A alloc)
		{
			sptr->reset(p, dtor, alloc);
		}

		T* get()
		{
			return sptr->get();
		}

		static T& operator*(ref_shared_ptr% rsp) { return *(*rsp->sptr); }

		T* operator->() { return sptr->get(); }

		long use_count() { return sptr->use_count(); }

		bool unique() { return sptr->unique(); }

		explicit operator bool()
		{
			return sptr->operator bool();
		}

		std::shared_ptr<T> const& toStd() { return *sptr; }


	private:
		std::shared_ptr<T> *sptr;

	};

}
