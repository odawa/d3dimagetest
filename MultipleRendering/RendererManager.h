#pragma once

#include <memory>
#include <d3d9.h>
#include "Renderer.h"
#include "ref_shared_ptr.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace System::Timers;

namespace MultipleRendering 
{
	public enum class Resolutions : int {
		Low,
		High,
		Mix
	};
	ref class RendererSlot
	{
	private:
		static System::Random^ m_random = gcnew System::Random();
		RendererBase^ m_renderer;
		Timer^ m_timer;
		List<MultipleRendering::FrameInfo^>^ m_frameInfoList;
		int m_frameIndex;

		void OnElapsed(Object^ sender, ElapsedEventArgs^ e);
		void RenderNext();
	public:
		RendererSlot(RendererBase^ renderer, double interval, List<MultipleRendering::FrameInfo^>^ frameInfoList);
		RendererBase^ GetRenderer();
	};

	public ref class RendererManager
	{
	private:
		IDirect3D9Ex* m_d3dEx;
		IDirect3DDevice9Ex* m_d3dDeviceEx;
		HWND hDeviceWindow;

		HRESULT EnsureD3D();
		HRESULT EnsureDevice();
		HWND CreateHWND();
		DWORD EnsureBehaviorFlags();

		String^ m_lastError = "";

		Resolutions m_resolution;

		Dictionary<RendererBase^, RendererSlot^>^ m_renderers;

		List<MultipleRendering::FrameInfo^>^ dummyFrameInfoList;

		void EnsureDummyFrames();
		void LoadFrames(String^ dirPath, int width, int height);
		void LoadFrame(String^ path, int width, int height, vrms5tools::ref_shared_ptr<std::vector<byte>>% buff);
	public:
		RendererManager(Resolutions resolution);
		bool Init();
		String^ GetLastError() { return m_lastError; }
		D3DRenderer^ NewD3DRenderer();
		WBRenderer^ NewWBRenderer();
		~RendererManager();
		!RendererManager();
	};
}
